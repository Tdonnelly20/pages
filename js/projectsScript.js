$(document).ready(function (){
    $.getJSON("../projects.json", function(data){
        var thumnail="";
        for (var i = 0; i < data.Projects.length; i++) {
            var projectData=data.Projects[i];
            var media='<div class=\"card__media card__media--large\">'+getMedia(projectData)+'</div>';

            var project = document.createElement('div');
            project.className = 'card click-container bg--white card--media-left';
            project.innerHTML =
                media +
                '<div class="card__body">' +
                '<h3><a href="'+projectData.project_page+'" >' + projectData.project_name + '</a></h3> <div class="card__details">' + getBody(projectData)+
                getButtons(projectData)+'</div></div></div>';
            //I gave the div the same ID's as the keys in the object for ease
            document.getElementById('gallery').appendChild(project);
        }
        $('.project_name').html(data.Projects[0].project_name)

    }).fail(function(){
        console.log("Damn that's whack")
    })
})

function getMedia(projectData){
    var width=$(window).width();
    var media;
    if(projectData.gallery_youtube_link==="-1"){
        if(width > 1260 || (width < 1024 && width > 425) || projectData.gallery_image_square===""){
            media='<img class="card__img" src="img/gallery-thumbnails/'+projectData.gallery_image+'" alt="'+projectData.project_name+' icon">';
        }else{
            media='<img class="card__img" style="aspect-ratio:1/1;" src="img/gallery-thumbnails/'+projectData.gallery_image_square+'" alt="'+projectData.project_name+' icon">';
        }
    }else{
        if( width < 1260 && (width > 1020 || width < 425) && projectData.gallery_image_square!==""){
            media='<img class="card__img" style="aspect-ratio:1/1;" src="img/gallery-thumbnails/'+projectData.gallery_image_square+'" alt="'+projectData.project_name+' icon">';
        }else{
            media='<div class="video_wrapper"><iframe class="card__video" src="'+projectData.gallery_youtube_link+'" title="'+projectData.project_name+'" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
        }
    }
    return media;
}


function getBody(data){
    let string=setBodyDetails("",data.project_publisher,"Publisher");
    string=setBodyDetails(string,data.project_role,"Role");
    string=setBodyDetails(string,data.project_tools,"Tools");
    string+='<p>'+data.project_gallery_desc+'</p>';
    if(data.project_contributions!=="" && data.project_contributions!=null) {
        string += '<h4>Contributions: </h4><p>' + data.project_contributions + '</p>';
    }else if(data.project_takeaways!=="" && data.project_takeaways!=null){
        string += '<h4>Key Takeaways: </h4><p>' + data.project_takeaways + '</p>';
    }
    return string;
}



function setBodyDetails(string,value,desc){
    if(value!=="" && value!=null){
        string+='<p class="details"><b>'+desc+': </b>'+value+'</p>';
    }
    return string;
}

function getButtons(data){
    let string='<div class="button-container"></div><div class="button"><a href="'+data.project_page+'">Read More</a>';
    for (var i in data.links) {
        string +=GetButton(data.links[i].url,data.links[i].text)
    }
    string+='</div></div>';
    return string;
}

function GetButton(value, buttonText){
    var string="";
    if(value!=="" && value!=null){
        string+='<a href="'+value+'">'+buttonText+'</a>';
    }
    return string;
}
