$(document).ready(function (){
    $.getJSON("../experience.json", function(data){
        for (var i = 0; i < data.Experience.length; i++) {
            var job=data.Experience[i];

            var project = document.createElement('div');
            project.className = 'experience';

            var header=GetHeader(job);
            var body=GetBody(job);
            project.innerHTML = header+body;
            //I gave the div the same ID's as the keys in the object for ease
            document.getElementById('projects').appendChild(project);
        }
        $('.project_name').html(data.Experience[0].project_name)

    }).fail(function(){
        console.log("Damn that's whack")
    })
})
function GetHeader(job){
    var string='<h4 style="text-align: center;">';
    if(job.header.link!==""){
        string+= '<u><a href="'+job.header.link+'">'+job.header.text+'</a></u>';
    }else{
        string+= job.header.text;
    }
    string+=' - '+job.header.position+' ('+job.header.duration+')</h4>';
    return string;
}

function GetBody(job){
    var string='<ul>';
    for(i in job.bullet_points){
        string+='<li>'+job.bullet_points[i].text+'</li>'
    }
    string+='</ul>';
    return HighlightBody(job,string);
}

function HighlightBody(job,body){
    var string=body;
    for(i in job.highlight){
        let text=job.highlight[i].text;
        if(text!==""){
            let newVersion;
            if(job.highlight[i].link==="" || job.highlight[i].link=== null){
                newVersion='<b>'+text+'</b>';
            }else{
                newVersion='<a href="'+job.highlight[i].link+'"><b><u>'+text+'</u></b></a>';
            }
            string=string.replaceAll(text,newVersion);
        }
    }
    console.log(string);
    return string;
}